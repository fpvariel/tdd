from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, friend_list, add_friend, delete_friend, validate_npm, fr_ls, model_to_dict
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.
class Lab7UnitTest(TestCase) :
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_friend_model(self):
		friend = Friend.objects.create(friend_name='Fakhril', npm='1606917531')
		counting_object = Friend.objects.all().count()
		self.assertEqual(counting_object,1)

	def test_friend_list(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertTemplateUsed(response, 'lab_7/daftar_teman.html')

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Fakhril", npm="123")
		response = Client().post('/lab-7/delete-friend/', {'id': friend.id})	
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(friend, Friend.objects.all())

	# def test_validate_npm_is_taken(self):
	# 	Friend.objects.create(friend_name="Fakhril", npm="123")
	# 	friend = Friend.objects.all()[0]
	# 	response = Client().post('/lab-7/validate-npm', {'npm' : friend.npm})
	# 	respDict = json.loads(response.content.decode('utf-8'))
	# 	self.assertTrue(respDict['is_taken'])

	# def test_validate_npm_not_taken(self):
	# 	response = Client().post('/lab-7/validate-npm', {'npm' : '123'})
	# 	respDict = json.loads(response.content.decode('utf-8'))
	# 	self.assertFalse(respDict['is_taken'])



