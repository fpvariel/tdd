from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
response['author'] = 'Fakhril'
csui_helper = CSUIhelper()
#os.environ.get("SSO_USERNAME", "yourusername"), os.environ.get("SSO_PASSWORD", "yourpassword")

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
	mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
	friend_list = Friend.objects.all()
	response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
	html = 'lab_7/lab_7.html'
	return render(request, html, response)

def friend_list(request):
	friendlist = Friend.objects.all()
	response['friend_list'] = friendlist
	html = 'lab_7/daftar_teman.html'
	return render(request, html, response)

@csrf_exempt
def add_friend(request):
	if request.method == 'POST':
		name = request.POST['name']
		npm = request.POST['npm']
		status = Friend.objects.filter(npm=npm).exists()
		if not status:
			friend = Friend(friend_name=name, npm=npm)
			friend.save()
			data = model_to_dict(friend)
			return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
	friend_id = request.POST['id']
	Friend.objects.filter(id=friend_id).delete()
	return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
	npm = request.POST.get('npm', None)
	friend = Friend.objects.all()
	data = {};
	data['is_taken'] = False
	for f in friend:
		if f.npm == npm :
			data['is_taken'] = True
			break
    
	return JsonResponse(data)

def fr_ls(request):
	friends = list(Friend.objects.all().values())
	data = {'fl':friends}
	return JsonResponse(data)
	
def model_to_dict(obj):
	data = serializers.serialize('json', [obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data
