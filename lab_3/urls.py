from django.conf.urls import url
from .views import add_activity
from .views import index

#url for app, add your URL Configuration

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'add_activity/$', add_activity, name='add_activity'),
]
