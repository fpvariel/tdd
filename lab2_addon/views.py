from django.shortcuts import render
    #Create a list of biodata that you wanna show on webpage:
    #[{'subject' : 'Name', 'value' : 'Igor'},{{'subject' : 'Birth Date', 'value' : '11 August 1970'},{{'subject' : 'Sex', 'value' : 'Male'}
    #TODO Implement
bio_dict = [{'subject' : 'Name', 'value' : 'Muhammad Fakhrillah Abdul Azis'},\
    {'subject' : 'Birth Date', 'value' : '5 December 1998'},\
    {'subject' : 'Sex', 'value' : 'Male'}]

def index(request):
    response = {'bio_dict' : bio_dict}
    return render(request, 'description_lab2addon.html', response)
   