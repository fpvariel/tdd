from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpResponseRedirect, HttpResponse
from .api_enterkomputer import get_drones
from .csui_helper import get_access_token, verify_user, get_client_id , get_data_user 
import requests
import environ
# Create your tests here.

env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class Lab9UnitTest(TestCase):
	#Test views===============================================================
	def test_url_is_exist(self):
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'lab_9/session/login.html')

	def test_index_with_user_login(self):
		s = self.client.session
		s['user_login'] = 'Asuna'
		s['access_token'] = '123abc'
		s['kode_identitas'] = 'kode rahasia'
		s['role'] = 'Player'
		s['drones'] = get_drones().json()
		s.save()
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code, 302)

	def test_profile_without_user_login(self):
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 302)

	def test_profile_with_user_login_and_drones(self):
		s = self.client.session
		s['user_login'] = 'Asuna'
		s['access_token'] = '123abc'
		s['kode_identitas'] = 'kode rahasia'
		s['role'] = 'Player'
		s['drones'] = get_drones().json()
		s.save()
		response = self.client.get('/lab-9/profile/')
		self.assertTemplateUsed(response, 'lab_9/session/profile.html')

	def test_profile_with_user_login_only(self):
		s = self.client.session
		s['user_login'] = 'Asuna'
		s['access_token'] = '123abc'
		s['kode_identitas'] = 'kode rahasia'
		s['role'] = 'Player'
		s.save()
		response = self.client.get('/lab-9/profile/')
		self.assertTemplateUsed(response, 'lab_9/session/profile.html')

	def test_add_drones_1(self):
		response = self.client.get(reverse('lab-9:add_session_drones', kwargs={'id':1}))
		self.assertEqual(response.status_code, 302)

	def test_add_drones_2(self):
		s = self.client.session
		s['drones'] = get_drones().json()
		response = self.client.get(reverse('lab-9:add_session_drones', kwargs={'id':1}))
		self.assertEqual(response.status_code, 302)

	def test_cookie_auth_login_success(self):
		dic = {'username' : 'fakhril', 'password' : 'pwpwpw'}
		response = self.client.post('/lab-9/cookie/auth_login/', dic)
		self.assertEqual(response.url, '/lab-9/cookie/login/')

	def test_cookie_auth_login_fail(self):
		dic = {'username' : 'Rize', 'password' : 'Tedeza'}
		response = self.client.post('/lab-9/cookie/auth_login/', dic)
		self.assertEqual(response.url, '/lab-9/cookie/login/')

	def test_cookie_auth_login_get(self):
		dic = {'username' : 'Rize', 'password' : 'Tedeza'}
		response = self.client.get('/lab-9/cookie/auth_login/', dic)
		self.assertEqual(response.url, '/lab-9/cookie/login/')

	def test_cookie_login_success(self):
		c = self.client.cookies
		c['username'] = 'fakhril'
		c['password'] = 'pwpwpw'
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 200)

	def test_cookie_login_fail(self):
		c = self.client.cookies
		response = self.client.get(reverse('lab-9:cookie_login'))
		self.assertTemplateUsed('lab_9/cookie/login.html')

	def test_cookie_profile_success(self):
		c = self.client.cookies
		c['username'] = 'fakhril'
		c['password'] = 'pwpwpw'
		response = self.client.post(reverse('lab-9:cookie_profile'))
		self.assertTemplateUsed('lab_9/cookie/profile.html')

	def test_cookie_profile_fail(self):
		c = self.client.cookies
		c['username'] = 'Rize'
		c['password'] = 'Tedeza'
		response = self.client.post(reverse('lab-9:cookie_profile'))
		self.assertTemplateUsed('lab_9/cookie/login.html')

	#Test costum_auth===================================================================
	def test_auth_login_success(self):
		dic = {}
		dic['username'] = env('SSO_USERNAME')
		dic['password'] = env('SSO_PASSWORD')

		response = self.client.post(reverse('lab-9:auth_login'), dic)
		self.assertEqual(response.status_code, 302)

	def test_auth_login_fail(self):
		dic = {}
		dic['username'] = 'oops'
		dic['password'] = 'kyaa'

		response = Client().post(reverse('lab-9:auth_login'), dic)
		self.assertEqual(response.status_code, 302)

	def test_auth_logout(self):
		dic = {}
		dic['username'] = env('SSO_USERNAME')
		dic['password'] = env('SSO_PASSWORD')

		response_login = Client().post(reverse('lab-9:auth_login'), dic)
		response_logout = Client().post(reverse('lab-9:auth_logout'))

		self.assertEqual(response_logout.url, reverse('lab-9:index'))

	#Test csui_helper=======================================================================
	def test_get_data_user(self):
		dic = {}
		dic['username'] = env('SSO_USERNAME')
		dic['password'] = env('SSO_PASSWORD')
		dic['npm'] = env('NPM')

		access_token = get_access_token(dic['username'], dic['password'])
		parameters = {'access_token' : access_token, 'client_id' : get_client_id()}
		API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"
		response = requests.get(API_MAHASISWA+dic['npm'], params=parameters)

		self.assertEqual(response.json(), get_data_user(access_token, dic['npm']))


		