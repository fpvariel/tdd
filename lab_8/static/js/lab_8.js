window.fbAsyncInit = () => {
    FB.init({
      appId      : '369297016857855',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    FB.getLoginStatus(function(response){
      render(response.status == 'connected')
    })

  };
  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  const render = loginFlag => {
    if (loginFlag) {
      getUserData(user => {
        if (user.cover) {
          console.log("Masuk")
          $('#lab8').html(
            '<div style="margin:0px 10px 0px 10px; padding:50px">' +
            '<hr> <h1 style="color:white;"> Profile </h1>' +
            '<div class="profile">' +
            '<img style="margin: 0px auto; display:block; background-size:cover" class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<br><br>' +
            '<img style="border-radius:50%;" class="fullpic" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<table style="width:30%">' +
                '<tr>' + 
                  '<th> Name </th>' + '<td>' + user.name + '</td>' +
                '</tr>' + 
                '<tr>' + 
                  '<th> About </th>' + '<td>' + user.about + '</td>' +
                '</tr>' +
                '<tr>' +
                  '<th> Email </th>' + '<td>' + user.email + '</td>' +
                '</tr>' +
                '<tr>' +
                  '<th> Gender </th>' + '<td>' + user.gender + '</td>' +
                '</tr>' +
              '</table>' +
            '</div>' +
          '</div>' +
          '<textarea rows="8" cols="50" id="postInput" style="resize:none"></textarea> <br>' +
          '<button class="postStatus" onclick="postStatus()">Post to Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>' +
          '</div>' + '<br><br>'
            );
        } else {
          console.log("Masuk2")
          $('#lab8').html(
            '<div style="margin:0px 10px 0px 10px; padding:50px">' +
            '<hr> <h1 style="color:white;"> Profile </h1>' +
            '<div class="profile">' +
            '<br><br>' +
            '<img style="border-radius:50%;" class="fullpic" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<table>' +
                '<tr>' + 
                  '<th> Name </th>' + '<td>' + user.name + '</td>' +
                  '<th> About </th>' + '<td>' + user.about + '</td>' +
                  '<th> Email </th>' + '<td>' + user.email + '</td>' +
                  '<th> Gender </th>' + '<td>' + user.gender + '</td>' +
              '</table>' +
            '</div>' +
          '</div>' +
          '<textarea rows="8" cols="50" id="postInput" style="resize:none"></textarea> <br>' +
          '<button class="postStatus" onclick="postStatus()">Post to Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>' +
          '</div>' + '<br><br>'
            );
        }
        getUserFeed(feed => {
          if (feed.data[0].story && feed.data[0].message) {
            if (feed.data[0].picture) {
              $('#lab8').append(
                '<div class="latest-feed-title" style="margin:0px 10px 0px 10px; padding:50px">' +
                  '<hr> <h1> Latest Feed </h1> <hr>' +
                  '<div class="latest-feed" style="background-color:white">' +
                    '<div class="feed-block1">' +
                      '<img style="width:120px; height:120px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '</div>' +
                    '<div class="feed-block1">' +
                      '<div class="story"> <h2>' + feed.data[0].story + '</h2> </div>' +
                      '<div class="date"> <h2>' + feed.data[0].created_time + '</h2> </div> <br><br>' +                      
                    '</div>' +
                    '<h3><hr>' + feed.data[0].message + '<h3>'+
                      '<button class="delete-post" onclick="deletePost(' + feed.data[0].id + ')">Delete Post </button>' +
                      '<br><br><div> <img class="feed-pic" src="' + feed.data[0].full_picture + '" alt="post-img" /> </div>' +
                  '</div>' + 
                '</div>'
              )
            } else {
              $('#lab8').append(
                '<div class="latest-feed-title" style="margin:0px 10px 0px 10px; padding:50px">' +
                  '<hr> <h1> Latest Feed </h1> <hr>' +
                  '<div class="latest-feed" style="background-color:white">' +
                    '<div class="feed-block1">' +
                      '<img style="width:100px; height:100px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '</div>' +
                    '<div class="feed-block1">' +
                      '<div class="story"> <h2>' + feed.data[0].story + '</h2> </div>' +
                      '<div class="date"> <h2>' + feed.data[0].created_time + '</h2> </div>' +                      
                    '</div><hr>' +
                    '<button class="delete-post" onclick="deletePost(' + feed.data[0].id + ')">Delete Post </button>' +
                    '<h3>' + feed.data[0].message + '<h3>'+
                  '</div>' + 
                '</div>'
              )
            }

            
          } else if (feed.data[0].story) {
            if (feed.data[0].picture) {
              $('#lab8').append(
                '<div class="latest-feed-title" style="margin:0px 10px 0px 10px; padding:50px">' +
                  '<hr> <h1> Latest Feed </h1>' +
                  '<div class="latest-feed" style="background-color:white">' +
                    '<div class="feed-block1">' +
                      '<img style="width:100px; height:100px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '</div>' +
                    '<div class="feed-block1">' +
                      '<div class="story"> <h2>' + feed.data[0].story + '</h2> </div>' +
                      '<div class="date"> <h2>' + feed.data[0].created_time + '</h2> </div>' +                      
                    '</div><hr>' +
                      '<button class="delete-post" onclick="deletePost(\'' + feed.data[0].id + '\')">Delete Post </button>' +
                      '<br><br><div> <img class="feed-pic" src="' + feed.data[0].full_picture + '" alt="post-img" /> </div>' +
                  '</div>' + 
                '</div>'
              )
            } else {
              $('#lab8').append(
                '<div class="latest-feed-title" style="margin:0px 10px 0px 10px; padding:50px">' +
                  '<hr> <h1> Latest Feed </h1>' +
                  '<div class="latest-feed" style="background-color:white">' +
                    '<div class="feed-block1">' +
                      '<img style="width:100px; height:100px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '</div>' +
                    '<div class="feed-block1">' +
                      '<div class="story"> <h2>' + feed.data[0].story + '</h2> </div>' +
                      '<div class="date"> <h2>' + feed.data[0].created_time + '</h2> </div>' +
                    '</div>' +
                    '<button class="delete-post" onclick="deletePost(' + feed.data[0].id + ')">Delete Post </button>' +
                  '</div>' + 
                '</div>'
              )
            }
            
          } else if (feed.data[0].message) {
            if (feed.data[0].picture) {
              $('#lab8').append(
                '<div class="latest-feed-title" style="margin:0px 10px 0px 10px; padding:50px">' +
                  '<hr> <h1> Latest Feed </h1> ' +
                  '<div class="latest-feed" style="background-color:white">' +
                    '<div class="feed-block1">' +
                      '<img style="width:100px; height:100px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '</div>' +
                    '<div class="feed-block1">' +
                      '<div class="story"> <h2>' + user.name + '</h2> </div>' +
                      '<div class="date"> <h2>' + feed.data[0].created_time + '</h2> </div>' +
                    '</div><hr>' +
                    '<button class="delete-post" onclick="deletePost(' + feed.data[0].id + ')">Delete Post </button>' +
                    '<h3>' + feed.data[0].message + '<h3>'+
                    '<br><br><div> <img class="feed-pic" src="' + feed.data[0].full_picture + '" alt="post-img" /> </div>' +
                  '</div>' + 
                '</div>'
              )
            } else {
               $('#lab8').append(
                '<div class="latest-feed-title" style="margin:0px 10px 0px 10px; padding:50px">' +
                  '<hr> <h1> Latest Feed </h1> ' +
                  '<div class="latest-feed" style="background-color:white">' +
                    '<div class="feed-block1">' +
                      '<img style="margin:0px width:100px; height:100px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '</div>' +
                    '<div class="feed-block1">' +
                      '<div class="story"> <h2>' + user.name + '</h2> </div>' +
                      '<div class="date"> <h2>' + feed.data[0].created_time + '</h2> </div>' +
                    '</div><hr>' +
                    '<button class="delete-post" onclick="deletePost(' + feed.data[0].id + ')">Delete Post </button>' +
                    '<h3>' + feed.data[0].message + '<h3>'+
                  '</div>' + 
                '</div>'
              )
            }
            
          }
        })    
      });

    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login" onclick="facebookLogin()" style="width:10%; height:10%">Login </button>');
    }
  }

  const facebookLogin = () => {
        FB.login(function(response){
          console.log("login status");
           console.log(response);
           location.reload()
        }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me,publish_pages'});
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
        FB.logout(function(response) {
          console.log(response)
          location.reload()
        });
    })
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
      FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name,email,picture.width(200).height(200),cover,gender,about', 'GET', function(response){
            console.log(response);
            fun(response);
          });
        }
    });
  };


  const getUserFeed = (fun) => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me/feed?fields=message,story,created_time,full_picture,picture', 'GET', function(response){
            console.log(response);
            fun(response);
          });
        }
    });
  };

  const postFeed = message => {
    FB.api("/me/feed", "POST", {"message" : message}, function (response) {
      if (response && !response.error) {
        console.log(response);
        location.reload()
      }
    });
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };

  const deletePost = (id) => {
    console.log(id)
    FB.api('/' + id,"DELETE", function(response){
      if (response.success) {
        location.reload()
      } else {
        alert("kok masih gagal?")
      }
    })
  }
