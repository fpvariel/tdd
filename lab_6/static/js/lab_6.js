// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x == 'ac') {
    /* implemetnasi clear all */
	print.value = '';
  } else if (x == 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

$(".send-button").click(function(){
	var input = $("textarea").val();
		if (input != "") {
			$("textarea").val("");
			$(".msg-insert").append('<p class="msg-send">' + input + '</p> <br>');
		}
})

$(document).ready(function(e) {
	$('.my-select').select2();
	$(".chat-input").keypress(function(e) {
		if(e.which == 13) {
			e.preventDefault();
			var input = $("textarea").val();
			if (input != "") {
				$("textarea").val("");
				$(".msg-insert").append('<p class="msg-send">' + input + '</p> <br>');
			}
		}
	})

	var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
}

$('.my-select').select2({
	'data':themes	
});

$('.apply-button').on('click', function(){  // sesuaikan class button
	var t = $(".my-select").val();
	
	for (var d = 0, len = themes.length; d < len; d += 1) {
		if (themes[d].id == t) {
			$("body").css("background-color", themes[d].bcgColor);
			$("footer p").css("color", themes[d].fontColor);
			localStorage.setItem('selectedTheme', JSON.stringify(themes[d]))
			
		}
	}
})
});






// END






















